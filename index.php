<?php

function getScript(){
	$script = file_get_contents("res/inc/script.js");
	$clean_script = str_replace("\n", "", $script);
	$clean_script = str_replace('"', "'", $script);
	return "javascript: ".$clean_script;
}

?>


<!DOCTYPE html>
<html lang="es">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="">
		<meta name="author" content="">
		<!-- <link rel="icon" href="../../favicon.ico"> -->

		<title>Sin Teclado Promerica</title>

		<!-- Bootstrap core CSS -->
		<link href="bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">

		<!-- Custom styles for this template -->
		<link href="res/css/jumbotron-narrow.css" rel="stylesheet">

		<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
			<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
	</head>

	<body>

		<div class="container">
			<div class="header">
				<ul class="nav nav-pills pull-right">
					<li class="active"><a href="#">Inicio</a></li>
					<li><a href="http://g3rcar.com">Contacto</a></li>
				</ul>
				<h3 class="text-muted">Sin Teclado</h3>
			</div>

			<div class="jumbotron">
				<h1>Prom&eacute;rica Online Sin Teclado Virtual</h1>
				<p class="lead">Desactiva el teclado virtual de la banca en l&iacute;nea del Banco Prom&eacute;rica El Salvador con este bookmarklet</p>
				<p><a class="btn btn-lg btn-success" href="<?php echo getScript(); ?>" role="button">Sin Teclado</a></p>
				<small>Arrastra el bot&oacute;n hasta tu barra de bookmarks y &uacute;salo la pr&oacute;xima vez que quieras 
				ingresar en la Banca Online para poder escribir con el teclado de tu computadora</small>
			</div>

			

			<div class="footer">

				<a href="http://bitbucket.org/gerardo_calderon/sin-teclado-bookmarklet"><img src="res/img/bitbucket.png" style="width:16px;height:16px;">C&oacute;digo fuente</a>
				<p>&copy; G3rcar 2014</p>
			</div>

		</div> <!-- /container -->

	</body>
</html>
